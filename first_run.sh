#!/bin/bash

# This is the one-liner tool which does any initialisation required to setup
# a freshly-installed scraper.
#
# Add your code at the end of the file.

THIS_SCRIPT=`readlink -f $0`
THIS_DIR=`dirname ${THIS_SCRIPT}`
REPO_DIR=${THIS_DIR}

cd ${REPO_DIR}
if [ -d "/home/output" ]; then                                                      
    echo "been run"                                                             
    exit 0                                                                      
fi                                                                              

if [ ! -d "venv" ]; then
    virtualenv --system-site-packages ~/venv
    echo 'source ~/venv/bin/activate' >> ~/.bash_profile
fi

source ~/venv/bin/activate
pip install --user -r requirements.txt

if [ "`crontab -l`" == "" ]; then
    echo "Installing crontab.txt"
    crontab crontab.txt
    
    echo "########## MAIL CONFIGURATION ##########"
    echo
    crontab -l |grep MAILTO
    echo
    echo "########################################"

else
    echo "WARNING: not overwriting your existing crontab. You need to manually"
    echo "         run: crontab tool/crontab.txt"
fi

echo "Now run: source ~/venv/bin/activate"

# Now we should have a working environment.
# 

# [ insert any scraper-specific initialisation code here ]
git clone https://bitbucket.org/scraperwikids/fp7-naf ~/fp7-naf
pip install --user -r ~/fp7-naf/requirements.txt
git clone https://bitbucket.org/scraperwikids/crawler ~/crawler
git clone https://github.com/scraperwiki/longrun ~/longrun
~/longrun/runme 
curl https://scraperwiki.com/api/status --data "type=ok\&message=Ready to scrape."
touch ~/incoming/demo.py
mkdir ~/output
ln -s ../output ~/http/output
