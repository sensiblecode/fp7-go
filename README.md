This process has been tested on Ubuntu.

## download

Following the scheme of the Data Services Template, we want to checkout this repository into the top level of a ScraperWiki box. 
We can't do this directly since it would mean checking out into an already occupied directory. Hence this little dance:

`
mkdir ~/BAK && mv ~/tool ~/incoming ~/http ~/BAK
`

`git clone https://bitbucket.org/scraperwikids/fp7-go`

or

`git clone git@bitbucket.org:scraperwikids/fp7-go.git`

if you prefer to use ssh.

Then shuffle the checked out contents into the top level:

```shell
cd fp7-go/

mv * ../

cd ..

rm –rf fp7-go
```

## install

`tool/first_run.sh`

This will download and install all of the requirements for the scraper. Once this has completed, activate
the virtual environment:

`source ~/venv/bin/activate`

## crawl

If this is being run on a ScraperWiki Platform box, then using `tmux` allows the process to be started and then left:

`tmux`

`cd tool/crawler/silkworm`

`scrapy crawl longlegs -a urls='["http://example.org"]' -a domains='["example.org"]' -o example.jsonlines -t jsonlines`

-a urls=: json list of urls to start at

-a domains=: json list of domains to restrict crawl to

-o: output filename

-t: output type

Once this is running the tmux can be detached using the key sequence:

Ctrl+B D (keystrokes which exit the tmux)

You can then return to inspect progress using:

`tmux attach`

## write date/author parser

Add a function called `example` which returns a dictionary per entry. Put it in `/tool/fp7-naf/plugins.py`

An example plugin looks like this:

```python
def chronicle(doc):
    import lxml.html
    root = lxml.html.fromstring(doc.content)
    parsed = {}

    try:
        creationtime = root.xpath("//meta[@property='article:published_time']/@content")[0]
    except IndexError:
        creationtime = None
    if creationtime:
        parsed['creationtime'] = creationtime

    try:
        author = ', '.join(root.xpath("//meta[@name='author']/@content"))
    except IndexError:
        author = None
    if author:
        parsed['author'] = author
    return parsed
```


## parse

`mv example.jsonlines ~/http`

`python ~/tool/fp7-naf/transform.py ~/http/example.jsonlines example`

The first argument to transform.py specifies the source data, the second argument is the name of the plugin. Output is as individual xml files in the `http` directory

