var install_success = function () {
    location.reload();
}

var install_failure = function(msg) {
    window.alert("Installation failed.")
    console.log(msg)
}

var loading_screen = function() {
    // TODO
}

if (typeof(LongRun) === 'undefined') {
    scraperwiki.exec("tool/first_run.sh", install_success, install_failure)
    loading_screen()
} else {


longrun = new LongRun();

var blowup = function (name, msg, state) {
    if (state == "not run") {
        set_status("ready and waiting to "+name)
        return
    }
    "use strict";
    set_status(name + " blew up");
};

var forget = function(){}

var showme = function(text) {
    "use strict";
    console.log(text);
    $('#scrape_status').text(text);
    $('#scrape_status').scrollTop(999999);
};

var set_status = function(text) {
    "use strict";
    console.log(text)
    $('#one_line_status').text(text);
};

var redirector = function(url) {
    "use strict";
    window.location.replace(url);
};

var generate_status_alert = function(text) {
    "use strict";
    var j = JSON.parse(text)
    var msg = j.lines + " pages scraped: " + (j.chars/Math.pow(1024,2)).toFixed(2) + " megabytes"
    set_status(msg);
    window.alert(msg);
};

var generate_vm_status_alert = function(text) {
    window.alert(text)
};

var show_status = function() {
    "use strict";
    scraperwiki.exec('tool/status', generate_status_alert, blowup);
};

var handle_enrunerate_response = function(response, success_function) {
    "use strict";
    if (response === 'running') {
        showme("RUN");
        // TODO spinners
        // try enrunify status again later.
        window.setTimeout(function() {get_enrunerate_status(success_function)}, 10000);
    } else if (response === 'success') {
        window.setTimeout(function() {success_function()}, 2000);
        // unlock and move tab
        // TODO problem for later
    } else if (response === 'failure') {
        showme("LOSE");
        // debug information
        // banner on screen?
    } else if (response === 'notrun') {
        showme("NOPE");
        // welcome message? first pass?
        // would be nice to save dialog info... :(
    } else {
        showme("FAIL\n"+response);
        // maybe 'notrun', or crazy message back
        // shouldn't get here.
        // basic error msg
    }
};

var get_enrunerate_status = function (success_function) {
    "use strict";
    scraperwiki.exec("tool/enrunerate", function(response) {handle_enrunerate_response(response, success_function)}, blowup);
};

var shellify = function(selector) {
    "use strict";
    var textarea = $(selector).val();
    if (typeof textarea === 'undefined' || textarea === "") {
        return '[]';
    } else {
        return scraperwiki.shellEscape(JSON.stringify(textarea.split('\n')));
    }
};

//get_enrunerate_status(); // no longer can assume what the last thing we ran was...

// OLD scraperwiki.exec("tool/first_run.sh", function(data){get_enrunerate_status(show_stdout_log)}, blowup);

var generic_poll = function(name, msg) {
    // TODO: make spinners?
    var current_date = Date()
    set_status(current_date + " polling "+name)
    return longrun._poll(name, msg)
}

////// first_run
var loaded = function(name, msg) {
    showme("Ready\n"+name+"\n"+msg)
    set_status("successfully first_run")
    longrun.register("crawl", crawled, blowup, generic_poll)
    longrun.register("save_boxes", saved_boxes, blowup, generic_poll)
    longrun.register("load_boxes", loaded_boxes, forget, generic_poll)
    longrun.get_one_state("crawl")
    longrun.start_and_get_state("load_boxes", "cat incoming/save_boxes.json")
    
}

var not_loaded = function(name, msg) {
    set_status("loading for the first time, please wait")
    longrun.start_and_get_state("first_run", "tool/first_run.sh")
}

longrun.register("first_run", loaded, not_loaded, generic_poll)
longrun.get_one_state("first_run")

var unspin = function(id) {
    $(id).removeClass('loading')
}

var crawled = function(name, msg) {
    set_status("successfully crawled")
    showme(name+"\n"+msg)
    longrun.register("nafify", naffed, blowup, generic_poll)
    longrun.get_one_state("nafify")
    unspin('#go_button')
}

var naffed = function(name, msg) {
    set_status("successfully naffed...")
    longrun.register("download_naf_zip", zipped_naf, blowup, generic_poll)
    longrun.register("download_json_zip", zipped_json, blowup, generic_poll)
    longrun.register("upload_to_vm", upload_to_vm, blowup, generic_poll)
    showme(msg)
    unspin('#process_button')
}

var zipped_naf = function(){
    redirector('output/naf.zip');
    unspin('#download_naf')
};

var zipped_json = function(){
    redirector('output/jsonlines.zip');
    unspin('#download_jsonlines')
};

var upload_to_vm = function(){
    set_status("Uploaded to VM successfully");
    unspin('#vm_upload_button')
};

var uploaded_python = function(){
    set_status("Successfully uploaded plugin");
    unspin('#upload-python')
}

var saved_boxes = function(){
    console.log("Saved box contents")
    unspin('#save_boxes')
}

var loaded_boxes = function(name, msg){
    set_status("Got fields")
    parsed = JSON.parse(msg)
    $.each(parsed, function(element, value) {
        $(element).val(value);
    });
}

var readSingleFile = function(evt) {
    //Retrieve the first (and only!) File from the FileList object
    var f = evt.target.files[0]; 

    if (f) {
      var r = new FileReader();
      r.onload = function(e) { 
	var contents = e.target.result;
        var text = scraperwiki.shellEscape(contents);
        var command = "echo "+text+" > ~/incoming/demo.py";
        set_status("Uploading plugin");
        scraperwiki.exec(command, uploaded_python, blowup)
      }
      r.readAsText(f);
    } else { 
      alert("Failed to load file");
    }
}

var save_boxes = function() {
    var save_structure = {};
    var boxes = ["#url_input", "#domain", "#allow_filter", "#deny_filter",
                 "#fileinput", "#plugin", "#test_page_URL",
                 "#vm_ip", "#vm_username", "#vm_password"];
    $.each(boxes, function(index, box) {
        save_structure[box] = $(box).val();
    });
    var save_text = scraperwiki.shellEscape(JSON.stringify(save_structure));
    $('#save_boxes').addClass('loading')//.html('Processing&hellip;');
    longrun.start_and_get_state("save_boxes", "tool/save_boxes.sh "+save_text);
}; 


$(function() {

    "use strict";

    $('#save_boxes').on('click', save_boxes);

    $('#go_button').on('click', function() {
        var command = [
                       ' ~/crawler/generic_crawler',
                       ' -a urls=',
                       shellify('#url_input'),
                       ' -a domains=',
                       shellify('#domain'),
                       ' -a allow=',
                       shellify('#allow_filter'),
                       ' -a deny=',
                       shellify('#deny_filter'),
                       ' -t jsonlines',
                       ' -o ~/output/out.jsonlines'].join('');
        $(this).addClass('loading')//.html('Searching&hellip;');
        set_status("Scraping website, this may take some time...");
        longrun.start_and_get_state("crawl", command)
        save_boxes();
    });

    $('#process_button').on('click', function() {
        var command = [' python ~/fp7-naf/transform.py',
                       ' ~/output/out.jsonlines ',
                       scraperwiki.shellEscape($('#plugin').val())
                      ].join('');
        $(this).addClass('loading')//.html('Processing&hellip;');
        set_status("Transforming to NAF...");
        longrun.start_and_get_state("nafify", command)
        // we've tried to make this log to a separate file: not working...
        save_boxes();
    });

    $('#status_button').on('click', function() {
        show_status();
    });

    $('#download_naf').on('click', function() {
        var command = 'tool/make_naf_zip';
        $(this).addClass('loading')//.html('Processing&hellip;');
        set_status("Zipping NAF");
        longrun.start_and_get_state("download_naf_zip", command)
        save_boxes();
    });

    $('#download_jsonlines').on('click', function() {
        var command = 'tool/make_jsonlines_zip';
        $(this).addClass('loading')//.html('Processing&hellip;');
        set_status("Zipping JSONLines");
        longrun.start_and_get_state("download_json_zip", command)
        save_boxes();
    });

    $('#upload-python').on('click', function() {
        var text = scraperwiki.shellEscape($('#python-code').val());
        var command = "echo "+text+" > ~/incoming/demo.py";
        set_status("Uploading plugin");
        scraperwiki.exec(command, uploaded_python, blowup)
        save_boxes();
    });

    $('#vm_upload_button').on('click', function() {
        //command: path might be incorrect TODO
        var command = ['tool/push_to_vm',
                       ' ~/output ',
                       scraperwiki.shellEscape($('#vm_ip').val()),
                       ' ',
                       scraperwiki.shellEscape($('#vm_username').val()),
                       ' ',
                       scraperwiki.shellEscape($('#vm_password').val()),
                      ].join('');
        $(this).addClass('loading');//.html('Processing&hellip;');
        set_status("starting upload to VM");
        // TODO: neied log file; maybe we can redirect as above, or maybe we need a script to generate it
        longrun.start_and_get_state("upload_to_vm", command);
        // we've tried to make this log to a separate file: not working...
        save_boxes();
    });

    $('#url_input').on('blur', function(){
      var urls = $.trim($(this).val()).replace(/https?:[/][/]/ig, '')
      var domains = $.trim($('#domain').val())
      urls = urls.replace(/[\/].*/, '')
      if(urls != '' && (domains == '' || domains == 'httpbin.org')){
        $('#domain').val(urls)
      }
    })
    
    $('#fileinput').on('change', function(evt){readSingleFile(evt)});

    $('#vm_status').on('click', function() {
        var command = 'curl -s ' +scraperwiki.shellEscape($('#vm_username').val()
     				       + ':'
				       + $('#vm_password').val()
				       + '@'
				       + $('#vm_ip').val())

        scraperwiki.exec(command, generate_vm_status_alert, blowup)
        save_boxes()
    })

    $('#scrape_status').on('click', function() {
        $(this).toggleClass("collapsed")
        $('#scrape_status').scrollTop(999999);
    })

    $('#test_button').on('click', function() {
        var command = [' python ~/fp7-naf/transform.py',
                       ' --url=',
                       scraperwiki.shellEscape($('#test_page_URL').val()),
                       ' ',
                       scraperwiki.shellEscape($('#plugin').val())
                      ].join('');
        scraperwiki.exec(command, showme, blowup);
        save_boxes()
    })
        
});

} // else
